
Printable Manuals
-----------------

This .zip file contains the PC-DMIS Core Help file and some other PC-DMIS help files as printable .pdf manuals.


== To Use ==
  1. In Windows 10, right-click on the .zip file and choose <<Extract All>>.
  2. Choose a destination folder and then click <<Extract>>.
  3. Once the files are extracted, open the folder to view the .pdf files.
  4. Double-click on a file and use a PDF reader to view its printable content.
  
  
== Limitations ==
Some Help files are not available as .pdf manuals.

The PC-DMIS Core printable .pdf manual does not have an index. 

== Recommendations ==
We recommend that you use the <<Index>> and <<Search>> tabs in the PC-DMIS Help Center to find what you need. You can access the Help Center from this link: http://docs.hexagonmi.com/pcdmis/2019.1/en/helpcenter/.

If you do need to search for something across multiple .pdf files, you can do that with Adobe Acrobat Reader DC. This free reader is available from Adobe:

  1. Once you download and install Acrobat Reader DC, go ahead and run it.
  2. From the <<Edit>> menu, choose <<Advanced Search>> to open the Search pane.
  3. From <<Where would you like to search?>>, choose <<All PDF Documents in>>.
  4. From the drop-down list, choose the folder that contains the extracted .pdf files.
  5. From <<What word or phrase would you like to search for?>>, type the word or phrase.
  6. Mark any search options and then click <<Search>>.
  7. If prompted by a security warning for read access to your drives, click <<Allow>>.
  8. From the <<Results>> list, expand the search results. Then click on the result you want to view in the .pdf file.

  
== About Localized Files ==
If this .zip file contains localized .pdf manuals, then your language supports printable .pdf manuals. If this .zip file is empty or if it contains only English .pdf manuals, then your language does not support printable .pdf manuals.
