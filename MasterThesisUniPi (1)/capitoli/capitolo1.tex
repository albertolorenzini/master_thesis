
In questo capitolo parlerò degli obiettivi dell'esperimento Belle II e a quali settori della ricerca nella fisica delle alte energie può dare un contributo. Nella seconda parte del capitolo invece introdurrò i concetti teorici alla base dell'analisi proposta in questa tesi nel capitolo 3.\\
\\
\\
\\
L'obiettivo primario di Belle II è quello di trovare evidenze di nuova fisica (NP) nel campo delle alte energie, in particolare nella fisica di sapore, oltre che aumentare la precisione dei parametri del Modello Standard. Per farlo Belle II, grazie al suo acceleratore SuperKEKB, si spinge ai limiti dell'intensità di eventi, con una macchina che ha già battuto i record di luminosità - registrando a fine dicembre 2021 il valore di $3.8\times10^{34} \mathrm{cm}^{-2}\mathrm{s}^{-1}$ - e che si prefigge di arrivare all'obiettivo di $8\times10^{35} \mathrm{cm}^{-2}\mathrm{s}^{-1}$. Belle II ha cominciato a raccogliere dati nel 2018.
\par
Il Modello Standard è una delle teorie fisiche con il più alto numero di conferme sperimentali. Tuttavia, il Modello Standard non dà spiegazione a tutta una serie di fenomeni o di caratteristiche naturali ancora inspiegabili, come ad esempio il fatto che esistano proprio tre famiglie di fermioni e che rispettino una gerarchia di massa precisa, oppure come sia possibile il livello di asimmetria tra materia e antimateria osservato, che con le misure fatte sulla violazione di $CP$ (di cui le B-factories come Belle II sono grandi protagoniste) non trova una spiegazione soddisfacente, essendo di vari ordini di grandezza inferiore rispetto all'universo osservato. Rimanendo legati alla violazione di $CP$, anche i valori sulla diagonale della matrice CKM (Cabibbo Kobayashi Maskawa) sono inspiegabilmente più alti rispetto a tutti gli altri. 
\par
Per cercare di dare una spiegazione a questi fenomeni, sono state elaborate tutta una serie di teorie di nuova fisica oltre il Modello Standard, per alcune delle quali Belle II cerca di dare una conferma, o una negazione, sperimentale. La sezione seguente è dedicata proprio a questi aspetti e a come Belle II può dare un contributo nella ricerca dei fenomeni che confermerebbero queste teorie.



\section{Programma di fisica a Belle II}

La più importante scoperta riguardante la fisica delle particelle fatta negli ultimi anni, è sicuramente quella del bosone di Higgs, fatta al CERN. La massa di 125 GeV del bosone di Higgs ha tuttavia alzato di molto i valori (in termini di energia) per cui ci si aspettava di vedere le prime evidenze predette dalla teoria supersimmetrica, la più promettente tra le teorie di nuova fisica, ai quali anche Belle II avrebbe potuto contribuire osservandole direttamente. Belle II, grazie alla sua luminosità e ad un ambiente molto più pulito rispetto ai rivelatori adronici, si è quindi concentrato sulla ricerca di canali specifici di eventi molto rari che possono dare evidenze indirette di nuova fisica, in particolar modo quelli che riguardano gli accoppiamenti di sapore.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=14cm]{images/belle2CAD.png}
    \caption{Disegno CAD del detector completo di Belle II. Questo detector rende possibile una precisa identificazione di particelle sia cariche che neutre, con energie che possono variare da qualche decina di MeV ad alcuni GeV. Come si può vedere dall'immagine il detector ha inoltre una copertura geometrica, in termini di angolo solido, quasi completa.}
    \label{fig:belle2CAD}
\end{figure}

La ricerca di eventi molto rari richiede una precisione elevatissima nella raccolta dati, più un evento è raro infatti e più diventa difficile da distinguere dal rumore di fondo, ecco perché per questo tipo di eventi un acceleratore a leptoni è sicuramente la scelta migliore. Tuttavia però, anche un rivelatore $e^+e^-$ come Belle II non è esente da tutta una serie di fenomeni che non possono essere classificati come rumore di fondo prodotto dal rivelatore (di cui parlerò nel capitolo 2), ma che vanno a rovinare la ricerca di eventi rari, come ad esempio tutte le particelle prodotte in un evento e che possono interagire con le particelle che compongono il materiale del rivelatore, andando a creare tutta una serie di eventi secondari indesiderati. 
\par
Per questo motivo è di fondamentale importanza una conoscenza il più approfondita possibile della geometria del rivelatore (mostrato in Fig. \ref{fig:belle2CAD}), ma non solo. Visto che una conoscenza perfetta è impossibile, per mitigare lo scattering multiplo è necessario mantenere un material budget il più basso possibile (dove le misure lo richiedono) in modo da perturbare le particelle il meno possibile ed inficiare così il meno possibile sulle misure che le riguardano. 
\par
Di queste misure che richiedono un material budget basso e una conoscenza della geometria dell'acceleratore approfondita, la più importante è sicuramente il tracking delle particelle, alla base di un acceleratore asimmetrico come quello di Belle II. Grazie a questa caratteristica infatti, tutte le misure dipendenti dal tempo possono essere riconvertite in misure di distanza relativa, ecco perché un'alta risoluzione del tracking (la cui descrizione verrà ripresa all'interno del capitolo 2) è di fondamentale importanza. Inoltre, l'analisi proposta al capitolo 3 di questa tesi ha proprio lo scopo di confrontare la geometria del rivelatore di vertice attuale VXD, dal punto di vista dei dati e del Monte Carlo, in modo da capire in quali punti le nostre previsioni vanno corrette.


\subsubsection{Esperimenti di fisica del sapore}
Il settore in cui Belle II si pone il maggior numero di obiettivi è sicuramente quello della fisica del sapore. Questo settore infatti permette di verificare molti parametri che potrebbero dare evidenze indirette di nuova fisica oltre il modello standard. Di seguito elencherò i più importanti argomenti di ricerca per cui Belle II è stato costruito \cite{physbook}.

\begin{description}
    \item[Violazione di $CP$] Dato che - come già accennato - la asimmetria tra materia e antimateria non è spiegabile con le conoscenze attuali di violazione di $CP$, Belle II sta cercando di osservare delle possibili discrepanze tra la frequenza di decadimento dei $B^0$ e dei $\overline{B}^0$ nei canali $b\rightarrow s$ e $b\rightarrow d$, come ad esempio i decadimenti $B\rightarrow \psi K^0$ e $B\rightarrow \eta' K^0$, dove ci si aspetta di vedere una violazione di $CP$ dipendente dal tempo, che potrebbe contribuire all'asimmetria osservata nell'Universo. Rimanendo sempre nel campo della violazione di $CP$, Belle II sta cercando di spiegare come mai i canali senza il quark charm (come ad esempio $B\rightarrow K \pi / K \pi \pi$), vìolino pesantemente $CP$ in maniera non dipendente dal tempo, come già osservato dalle precedenti B-factories e anche da LHCb.
    \item[Bosone di Higgs] Alcuni modelli di nuova fisica includono dei bosoni di Higgs carichi, oltre a quello neutro già osservato. Una verifica indiretta della effettiva esistenza di questi Higgs carichi si può fare osservando le transizioni di sapore nel leptone $\tau$, in particolare nei canali di decadimento $B\rightarrow \tau \nu$ oppure $B\rightarrow D^{(*)} \tau \nu$. Queste ricerche sono particolarmente promettenti in quanto delle deviazioni dal Modella Standard sono già state osservate con una deviazione di $3\sigma$, Belle II potrebbe quindi confermare definitivamente queste osservazioni.
    \item[FCNC] i Flavour-Changing-Neutral-Current, sono fenomeni di particolare interesse. Nella matrice CKM sono infatti pesantemente soppressi, ci si aspetta quindi che all'interno di questi fenomeni ci possano essere degli elementi che possano indicare nuova fisica oltre il Modello Standard. Di particolare interesse all'interno di questi fenomeni c'è l'osservazione del quark $c$: Il quark $c$ è infatti l'unico quark del settore up, in grado di decadere debolmente e dare quindi qualche informazione rilevante.
    \item[LFV] Lepton-Flavour-Violation. Belle II è in grado di produrre una moltidudine senza precedenti di leptoni $\tau$. Dato che gli esperimenti sui neutrini hanno dimostrato un livello alto di mixing tra il $\nu_{\tau}$ e il $\nu_{\mu}$, ci si chiede quindi se una violazione del sapore leptonico, attraverso un decadimento come ad esempio $\tau\rightarrow\mu\gamma$ sia possibile. I modelli che spiegano la creazione della massa dei neutrini infatti, predicono che questo tipo di decadimento dovrebbe succedere con una frequenza dell'ordine di $10^{-8}$, Belle II, nell'arco della sua presa dati, dovrebbe essere in grado di osservare e distinguere questi eventi, nel caso esistano, con quel livello di rarità.
\end{description}


\subsubsection{Ulteriori campi di ricerca}
Belle II tuttavia, può contribuire anche in campi della ricerca che non sono strettamente correlati alla fisica del sapore. Alcuni fenomeni di fisica fondamentale che non hanno ancora trovato una spiegazione soddisfacente infatti, possono beneficiare dell'ambiente di osservazione estremamente pulito che un rivelatore ad $e^+e^-$ come quello di Belle II è. In particolare i settori in cui Belle II si propone di migliorare le conoscenze attuali sono:

\begin{description}
    \item[Materia oscura] Belle II è pensato per essere particolarmente preciso nel misurare le energie mancanti dai decadimenti osservati. La maggior parte delle ricerche per cui è stato pensato Belle II infatti includono la misurazione indiretta di fenomeni rari, quindi anche tutto ciò che è "nascosto" dovrebbe poter essere notato, se è all'interno dei valori di energia in cui Belle II opera. Inoltre il nuovo sistema di trigger di Belle II è in grado di attivarsi anche al passaggio di un singolo fotone, permettendo quindi di verificare la possibile esistenza di particelle particolarmente difficili da osservare, come ad esempio possono essere gli assioni.
    \item[QCD] Lo studio dei quorkoni come il charmonio e il bottomonio è considerato un argomento di grande importanza a Belle II. Grazie agli studi fatti dalle precedenti B-factories e dagli acceleratori adronici, si è capito che questi particolari mesoni, sono di grande interesse per la comprensione della cromodinamica quantistica. Belle II può produrre quarkoni con una precisione elevatissima, in un ambiente estremamente pulito. Inoltre, la capacità di identificare precisamente particelle sia cariche che neutre e la sua copertura quasi totale dell'angolo solido intorno al punto di interazione, lo rende particolarmente indicato per questo genere di ricerche.
\end{description}

Oltre a questi aspetti di ricerca di nuova fisica, Belle II si pone anche l'obiettivo di aumentare la precisione nella misura di moltissimi valori che hanno contribuito alle numerose conferme del Modello Standard. Infine, anche le caratteristiche delle particelle conosciute possono sempre essere migliorate, aumentarne il livello di precisione infatti, permette a tutta la comunità di ricerca nelle alte energie di sviluppare le loro singole analisi. 
\par
Nel capitolo 2 spiegherò più nel dettaglio le caratteristiche del rivelatore di Belle II, che rende realizzabili tutte gli obiettivi descritti in questa sezione.


\section{Interazione dei Fotoni con la Materia}
In questa sezione introdurrò i concetti teorici alla base dell'analisi proposta nel capitolo 3. Essendo l'analisi basata sulla ricostruzione dei vertici in cui vengono prodotte coppie elettrone positrone, parlerò essenzialmente dell'interazione dei fotoni con la materia. Nella prima parte instrodurrò in maniera superficiale l'effetto fotoelettrico e lo scattering Compton, che mi serviranno per introdurre il fenomeno di produzione di coppie da parte di fotoni ad alta energia, che come vedremo è l'unico fenomeno di interazione radiazione-materia di reale interesse per questa tesi, essendo l'unico utilizzato nell'analisi. 
\subsubsection{Effetto fotoelettrico}
Quando un fotone a bassa energia interagisce con un materiale, gli elettroni degli atomi che lo compongono possono assorbirlo e, se l'energia del fotone è sufficiente, l'elettrone può vincere la forza di legame ed essere quindi emesso, dando vita all'effetto fotoelettrico $\gamma + \mathrm{atom} \rightarrow \mathrm{atom}^+ + e^-$. L'energia del fotone emesso è data da $E_e = h\nu - h \nu_0$, dove $h\nu_0$ è l'energia di legame dell'elettrone mentre $h\nu$ è l'energia del fotone incidente. Tuttavia, la cross-section dell'effetto fotoelettrico diminuisce rapidamente all'aumentare dell'energia del fotone incidente e già ad energie superiori ai 500 keV diventa trascurabile. Rimanendo in questo ordine di energie, una formula approssimata che può essere utilizzata per calcolare la cross-section per energie maggiori dell'energia di ionizzazione dell'atomo ma minori di $m_e c^2$ è la seguente:

$$\sigma_{pe} \simeq 4\alpha^2 \sqrt{2}Z^5\sigma_{Th}\biggl(\frac{m_ec^2}{h\nu}\biggr)^{7/2} $$

dove $\sigma_{Th} = 8\pi r_e^2/3 = 6.65 \cdot 10^{-25} \mbox{cm}^2$ è la cross-section totale dello scattering Thomson, ovvero per i processi elastici con elettroni liberi $\gamma e^- \rightarrow \gamma e^-$ con energie del fotone vicine allo zero, in cui si può quindi trascurare il momento dell'elettrone dopo l'urto. $r_e$ è il raggio classico dell'elettrone e $\alpha = 1/137$ è la costante di struttura fine, infine $Z^5$ è il numero atomico (elevato alla quinta potenza) del materiale con cui il fotone interagisce \cite{braibant}. 

\begin{figure}[htbp]
    \centering
    \includegraphics[width=14cm]{images/photon_interactions.png}
    \caption{Cross-section dell'interazione dei fotoni con il piombo. Come si può vedere, nel caso del piombo, per energie comprese tra $\sim 500$ keV e $\sim 5$ MeV, l'interazione più probabile è lo scattering Compton, mentre al di sotto è dominante l'effetto fotoelettrico, all'aumentare dell'energia invece, la produzione di coppia diventa il processo che contribuisce maggiormente alla cross-section, rendendo gli altri fenomeni trascurabili \cite{yao}.}
    \label{fig:photon_interactions}
\end{figure}


\subsubsection{Scattering Compton}

Aumentando l'energia, ma rimanendo sotto la soglia di produzione di coppie $e^+e^-$, l'interazione più probabile diventa lo scattering Compton. Lo scattering Compton è una collisione elastica di un fotone con un elettrone $\gamma e^- \rightarrow \gamma e^-$. Se il fotone incidente ha un'energia di molto superiore all'energia di legame dell'elettrone, quest'ultimo può essere considerato come libero. Per ricavare la la cross-section totale dello scattering Compton si può partire dalla formula di Klein-Nishina:

$$\frac{d\sigma_C}{d\Omega} = \frac{r_e^2}{2} \frac{1}{[1+\Gamma(1-cos\theta)]^2} \bigg( 1 +cos^2\theta +  \frac{\Gamma^2(1-cos\theta)^2}{1+\Gamma(1-cos\theta)} \bigg)$$

Dove $\Gamma = h\nu/m_ec^2$, in cui $h\nu$ è ancora l'energia del fotone incidente, così come $r_e$ è il raggio classico dell'elettrone.
\par
Integrando la formula di Klein-Nishina su tutto l'angolo solido abbiamo la sezione d'urto totale dello scattering Compton:

$$\sigma_C = 2\pi r_e^2 \biggl[ \frac{1+\Gamma}{\Gamma^2}\biggl(\frac{2(1+\Gamma)}{1+2\Gamma} - \frac{1}{\Gamma}\mathrm{ln}(1+2\Gamma)\biggr) + \frac{1}{2\Gamma}\mathrm{ln}(1+2\Gamma)-\frac{1+3\Gamma}{(1+2\Gamma)^2}$$

Dove per $\Gamma \ll 1$ si torna alla sezione d'urto dello scattering Thomson citata nella sottosezione sull'effetto fotoelettrico.
\par
A questo punto, superando la soglia di energia della produzione di coppia, quest'ultima diventa un fenomeno da inserire nella cross-section totale, e dai 5-10 MeV diventa il fenomeno predominante. Visto che è l'unica interazione dei fotoni con la materia utilizzata nell'analisi del capitolo 3 - dove farò anche alcune assunzioni basate sulla sua cross-section -, la tratterò a parte nella prossima sezione dedicata.


\subsection{Produzione di Coppie}

In questa sezione introdurrò il fenomeno della produzione di coppia da parte di fotoni ad alta energia e della sua sezione d'urto. Tuttavia, un calcolo generale per la sezione d'urto nel caso della produzione di coppie non è d'interesse per questa tesi, oltre che molto complicato. Nella seconda parte mi soffermerò sulla formula specifica della cross-section che utilizzerò nel capitolo dedicato all'analisi del material budget di VXD, spiegando i ragionamenti che ci sono dietro alla scelta di quella specifica formula.
\par
Il fenomeno della produzione di coppia da parte di un fotone consiste nella conversione di un fotone in una coppia $e^+e^-$, attraverso l'interazione elettromagnetica con un altro oggetto, che solitamente è il nucleo di un atomo, $\gamma+Z\rightarrow Z+e^++e^-$. Il diagramma di Feynamn che descrive la trasformazione è mostrato in fig. \ref{fig:pairprod}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=5cm]{images/pairprod.png}
    \caption{Diagramma di Feynman del processo di produzione di coppia.}
    \label{fig:pairprod}
\end{figure}

Nella mia tesi, il processo di produzione di coppia è alla base dell'analisi proposta al capitolo 3, in cui confronto il material budget del rivelatore VXD dal punto di vista del Monte Carlo e dei dati. Questo confronto lo faccio relativamente alla lunghezza di radiazione che come definizione utilizza la perdita di energia di elettroni attraverso radiazione Bremsstrahlung o la produzione di coppie $e^+e^-$ da parte di un fotone energetico. In particolare per la produzione di coppia, la definizione di lunghezza di radiazione $X_0$ corrisponde ai 7/9 del cammino libero medio di un fotone ad alta energia che converte in una coppia $e^+e^-$ all'interno del materiale.
\par
Il primo aspetto da tenere in considerazione è la quantità di energia di soglia $E_{\gamma S}$ necessaria ad un fotone per creare una coppia $e^+e^-$:

$$E_{\gamma S} = 2\biggl(m_e+\frac{m_e}{m_r}\biggr)$$

Dove $m_r$ è la massa della particella bersaglio. La quantità di energia richiesta quindi varia in base alla massa della particella bersaglio, nel mio caso il materiale considerato con cui il fotone interagirà nella produzione di coppie $e^+e^-$ è stato semplificato in carbonio e silicio, che ricordo avere un numero atomico rispettivamente di $Z_C = 6$ e $Z_{Si} = 14$, come si vedrà nel capitolo 3, le selezioni fatte in energia, sono molto al di sopra dell'energia di soglia necessaria alla produzione di coppie $e^+e^-$.
\par
Nel processo di formazione di coppie è conveniente dividere due differenti situazioni, una prima situazione in cui il momento della particella bersaglio dopo l'urto è trascurabile rispetto alla sua energia totale, mentre una seconda situazione in cui l'energia del fotone incidente è paragonabile con la massa della particella bersaglio e quindi il momento di quest'ultima dopo l'urto è da tenere in considerazione.
\par
La cross-section del processo di produzione di coppia ha una forma estremamente complicata e non è nell'interesse di questa tesi approfondire il calcolo di essa. Tuttavia, ritengo utile spiegare il ragionamento dietro alla scelta della formula utilizzata nel capitolo 3. Per la produzione di coppia esistono infatti numerose approssimazioni possibili che permettono di ricavare delle formule semplificate per la sezione d'urto in base al caso specifico in esame. Nella sottosezione seguente spiegherò le approssimazioni utilizzate e introdurrò la formula corrispondente.



\subsubsection{cross-section $e^+e^-$ di interesse per questa tesi}
Senza andare ad approfondire il calcolo della cross-section (per chi lo volesse, le formule della cross-section sono state calcolate per tutti i tipi di approssimazioni da Motz, Olsen e Koch nel loro articolo sulla produzione di coppie da parte dei fotoni \cite{ppphotons}), ci poniamo in una situazione in cui possiamo utilizzare l'approssimazione di Born, ovvero il caso in cui $\alpha Z/\beta_+,\beta_- \ll m_e c^2$. Infatti, l'energia dei fotoni verrà selezionata sopra ai 100 MeV, possiamo quindi considerare $\beta_+,\beta_- \sim 1$, inoltre ricordando che $\alpha=1/137$ e che $Z_C = 6$ e $Z_{Si} = 14$, possiamo considerarci all'interno dell'approssimazione. La seconda approssimazione è quella di considerare trascurabile il momento del nucleo bersaglio dopo l'urto. Partendo da queste approssimazioni e rimandendo nell'ambito di energie ultra-relativistiche $E_+,E_-,\gamma \gg m_e c^2$, la formula della cross-section, dove l'elettrone riceve una frazione dell'energia del fotone $x=E_-/E_{\gamma}$, è:

$$\frac{d\sigma_{pp}}{dx} = \alpha r_e^2 \bigg\{\biggl(\frac{4}{3}x(x-1)+1\biggr) \biggl[Z^2(\phi_1(\eta)-\frac{4}{3}\mathrm{ln}Z-4f(Z))+Z(\psi_1(\eta)-\frac{8}{3}\mathrm{ln}Z)\biggr]$$
$$+\frac{2}{3}x(1-x)[(Z^2(\phi_1(\eta)-\phi_2(\eta))+Z(\psi_1(\eta)-\psi_2(\eta))]\bigg\}$$

in cui $f(Z)$ è la correzione di Coulomb, data dalla seguente espansione in serie di $a=\alpha Z$:

$$f(Z) = a^2 \sum_{n=1}^{\infty} \frac{1}{n(n^2+a^2)},$$

mentre $\phi_1(\eta),\phi_2(\eta)$ e $\psi_1(\eta),\psi_2(\eta)$ sono le funzioni di screening, rispettivamente per lo scattering del nucleo e dei singoli elettroni, dove $\eta$ è il parametro di screening:

$$\eta = \frac{137m_ec^2E_{\gamma}}{2E_+E_-Z^{1/3}}.$$
\par
A questo punto introduciamo un'ulteriore approssimazione, quella in cui l'atomo bersaglio può essere visto come completamente schermato, ovvero $\alpha Z^{1/3}k \gg m_e c^2$. Nel nostro caso infatti $Z_C^{1/3} = 1.82$ mentre $Z_{Si}^{1/3} = 2.41$, con $\alpha = 1/137$, significa che possiamo utilizzare questa approssimazione per energie del fotone incidente superiori a $\sim 330$ MeV. Nel caso completamente schermato $\eta\rightarrow0$ e le funzioni di screening diventano:

$$\phi_1(0)-\phi_2(0) = \psi_1(0)-\psi_2(0) = \frac{2}{3}$$

Con questa ulteriore approssimazione, la formula della cross-section si semplifica a:

$$\frac{d\sigma_{pp}}{dx} = 4\alpha r_e^2\bigg\{\biggl(\frac{4}{3}x(x-1)+1\biggr)[Z^2(L_{rad}-f(Z))+ZL_{rad}']+\frac{1}{9}x(1-x)(Z^2+Z)\bigg\}$$

Dove $L_{rad}=1/4\phi_1(0)-1/3\mathrm{ln}Z$ e $L'_{rad} = 1/4\psi_1(0)-2\mathrm{ln}Z$. \\
Integrando questa formula otteniamo la cross-section totale:

$$\sigma_{pp} = 4\alpha r_e^2\frac{7}{9} \bigg\{[Z^2(L_{rad}-f(Z))+ZL'_{rad}]+\frac{1}{42}(Z^2+Z) \bigg\}$$

che come si può vedere non dipende dall'energia del fotone, rimanendo con energie superiori ai $> 330$ MeV. 
\par
Infine, bisogna anche tenere in considerazione che nel sistema di riferimento del laboratorio l'energia tra positroni ed elettroni non è equamente distribuita ($E_+\neq E_-$). In questi casi, per energie $E_-/E_{\gamma}$ vicine a 0 o ad 1 la cross-section non è costante in $x=E_-/E_{\gamma}$, come mostrato nella Fig. \ref{fig:ElecsuEgamma}. Come vedremo nel capitolo 3 la distribuzione $E_-/E_{\gamma}$ rende trascurabile questo fenomeno.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=14cm]{images/sigmappp.png}
    \caption{Cross-section differenziale della produzione di coppie, in unità $\alpha Z^2 r_0^2/E_{\gamma}$, in funzione dell'energia normalizzata dell'elettrone $x=E_-/E_{\gamma}$. Le curve a 50 MeV, 100 MeV e 1 GeV, sono fatte assumendo uno screening non completo. La curva asintotica (Con $E_{\gamma}=\infty$) è fatta assumendo uno screening completo \cite{kolanoski}}
    \label{fig:ElecsuEgamma}
\end{figure}

