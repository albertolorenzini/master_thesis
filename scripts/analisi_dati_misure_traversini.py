import numpy as np
import logging
import matplotlib.pyplot as plt
import seaborn as sns
import os

#carico i file e li inserisco dentro a delle matrici dentro ad un dizionario, divide tra quelli da 1mm e da 0.5mm
dirs = os.listdir('../structure_items_measurements/raw data/')
matrix05 = {}
matrix10 = {}
k = 0
j = 0

for file in dirs:
    if file[3] == "5":
        matrix05[k] = np.loadtxt('../structure_items_measurements/raw data/%s' % file, usecols=(1,2,3), skiprows=7, delimiter=',')
        k += 1
    if file[3] == "0":
        matrix10[j] = np.loadtxt('../structure_items_measurements/raw data/%s' % file, usecols=(1,2,3), skiprows=7, delimiter=',')
        j += 1

#carico i pesi e gli spessori dei vari traversini
matrix_pesi_10 = np.loadtxt('../structure_items_measurements/weights/pesi_travetti_10.txt', usecols=1, delimiter=',')
matrix_spessori_10 = np.loadtxt('../structure_items_measurements/thickness/spessori_travetti_10.txt', usecols=(1,2,3), delimiter=',')
matrix_pesi_05 = np.loadtxt('../structure_items_measurements/weights/pesi_travetti_05.txt', usecols=1, delimiter=',')
matrix_spessori_05 = np.loadtxt('../structure_items_measurements/thickness/spessori_travetti_05.txt', usecols=(1,2,3), delimiter=',')

#estraggo la larghezza di taglio dalle matrici e ne calcolo media e la std, prima dei traversini da 1mm di larghezza e poi da quelli da 0.5mm
differenze_totali_larghezza_10 = {}
angolo_gradi_10 = []
larghezza_lato_a_media10 = []
larghezza_lato_b_media10 = []

for key in matrix10:
    #differenza coordinate X
    larghezza_lato_a_10 = []
    larghezza_lato_b_10 = []
    larghezza_lato_a_10.append(abs(matrix10[key][5][0] - matrix10[key][6][0]))
    larghezza_lato_a_10.append(abs(matrix10[key][8][0] - matrix10[key][9][0]))
    larghezza_lato_b_10.append(abs(matrix10[key][12][0] - matrix10[key][13][0]))
    larghezza_lato_b_10.append(abs(matrix10[key][14][0] - matrix10[key][15][0]))
    larghezza_lato_a_media10.append((larghezza_lato_a_10[0]+larghezza_lato_a_10[1])/2)
    larghezza_lato_b_media10.append((larghezza_lato_b_10[0]+larghezza_lato_b_10[1])/2)
    #differenza larghezza tra i due lati
    differenza_larghezza_10_controllato = []
    differenza_larghezza_10 = []
    differenza_larghezza_10.append(abs(larghezza_lato_a_10[0] - larghezza_lato_b_10[0]))
    differenza_larghezza_10.append(abs(larghezza_lato_a_10[1] - larghezza_lato_b_10[1]))
    i=0
    for i in range(len(differenza_larghezza_10)):
        if(differenza_larghezza_10[i] > 0.5 and differenza_larghezza_10[i] < 0):
            logging.warning("qualcosa è andato storto in questa misura, la differenza tra i due lati è %f \nQuesto valore verrà scartato" % differenza_larghezza_10[i])
        else:
            differenza_larghezza_10_controllato.append(differenza_larghezza_10[i])
    differenze_totali_larghezza_10[key] = np.mean(differenza_larghezza_10_controllato)
    angolo = np.arctan(differenze_totali_larghezza_10[key]/matrix_spessori_10.mean(1)[key])
    angolo_gradi_10.append(np.degrees(angolo))
    #print(angolo_gradi_10[key])

#0.5 mm
differenze_totali_larghezza_05 = {}
angolo_gradi_05 = []
larghezza_lato_a_media05 = []
larghezza_lato_b_media05 = []

for key in matrix05:
    #differenza coordinate X
    larghezza_lato_a_05 = []
    larghezza_lato_b_05 = []
    larghezza_lato_a_05.append(abs(matrix05[key][5][0] - matrix05[key][6][0]))
    larghezza_lato_a_05.append(abs(matrix05[key][8][0] - matrix05[key][9][0]))
    larghezza_lato_b_05.append(abs(matrix05[key][12][0] - matrix05[key][13][0]))
    larghezza_lato_b_05.append(abs(matrix05[key][14][0] - matrix05[key][15][0]))
    larghezza_lato_a_media05.append((larghezza_lato_a_05[0]+larghezza_lato_a_05[1])/2)
    larghezza_lato_b_media05.append((larghezza_lato_b_05[0]+larghezza_lato_b_05[1])/2)
    #differenza larghezza tra i due lati
    differenza_larghezza_05 = []
    differenza_larghezza_05_controllato = []
    differenza_larghezza_05.append(abs(larghezza_lato_a_05[0] - larghezza_lato_b_05[0]))
    differenza_larghezza_05.append(abs(larghezza_lato_a_05[1] - larghezza_lato_b_05[1]))
    i=0
    for i in range(len(differenza_larghezza_05)):
        if(differenza_larghezza_05[i] > 0.5 and differenza_larghezza_05[i] < 0):
            logging.warning("qualcosa è andato storto in questa misura, la differenza tra i due lati è %f \nQuesto valore verrà scartato" % differenza_larghezza_05[i])
        else:
            differenza_larghezza_05_controllato.append(differenza_larghezza_05[i])
    
    differenze_totali_larghezza_05[key] = np.mean(differenza_larghezza_05_controllato)
    #da decommentare una volta fatte anche le misure da 05
    angolo = np.arctan(differenze_totali_larghezza_05[key]/matrix_spessori_05.mean(1)[key])
    angolo_gradi_05.append(np.degrees(angolo))
    print(angolo_gradi_05[key])

#print(differenze_totali_larghezza_10)
#print(differenze_totali_larghezza_05)


#calcolo la geometria dei traversini, prima quelli da 1mm poi da 0.5mm

dimensione_lato_x_10 = {}
dimensione_lato_y_10 = {}

for key in matrix10:
    #azzero i vari contatori e variabili locali
    j=0
    i=0
    x_10 = [0, 0, 0, 0]
    y_10 = [0, 0]
    x_y_10 = [0, 0]
    y_x_10 = [0, 0]
    x_10_controllato = []
    y_10_controllato = []

    #calcolo la differenza in X
    x_10[0] = (abs(matrix10[key][1][0] - matrix10[key][2][0]))
    x_10[1] = (abs(matrix10[key][3][0] - matrix10[key][4][0]))
    x_10[2] = (abs(matrix10[key][5][0] - matrix10[key][10][0]))
    x_10[3] = (abs(matrix10[key][7][0] - matrix10[key][8][0]))
    #calcolo la differenza in y per correggere la differenza in x (pitagora) con y_x si intende y di x
    y_x_10[0] = (abs(matrix10[key][1][1] - matrix10[key][2][1])) 
    y_x_10[1] = (abs(matrix10[key][3][1] - matrix10[key][4][1]))
    for j in range(len(x_10)):
        #controllo che le dimensioni non sfanculino
        if (x_10[j] < 36.5) and (x_10[j] > 35.5):
            x_10_controllato.append(np.sqrt(x_10[j]**2+np.mean(y_x_10)**2))
        #se la dimensione del lato non ha senso esco l'errore
        else:
            logging.warning("la misura del lato X è fuori dai parametri stabiliti (35.5 - 36.5), il suo valore è: %f \n Questo valore verrà scartato" % x_10[j])
    dimensione_lato_x_10[key] = np.mean(x_10_controllato)

    #calcolo la differenza in Y
    y_10[0] = (abs(matrix10[key][1][1] - matrix10[key][4][1]))
    y_10[1] = (abs(matrix10[key][2][1] - matrix10[key][3][1]))
    #calcolo la differenza in x per correggere la differenza in y (pitagora)
    x_y_10[0] = (abs(matrix10[key][1][0] - matrix10[key][4][0])) 
    x_y_10[1] = (abs(matrix10[key][2][0] - matrix10[key][3][0]))
    for i in range(len(y_10)):
        #controllo che le dimensioni non sfanculino
        if (y_10[i] < 705) and (y_10[i] > 703):
            y_10_controllato.append(np.sqrt(y_10[i]**2+np.mean(x_y_10)**2))
        #se la dimensione del lato non ha senso esco l'errore
        else:
            logging.warning("la misura del lato Y è fuori dai parametri stabiliti (703 - 705), il suo valore è: %f \n Questo valore verrà scartato" % y_10[i])
    dimensione_lato_y_10[key] = np.mean(y_10_controllato)

#0.5mm

dimensione_lato_x_05 = {}
dimensione_lato_y_05 = {}

for key in matrix05:
    #azzero i vari contatori e variabili locali
    j=0
    i=0
    x_05 = [0, 0, 0, 0]
    y_05 = [0, 0]
    x_y_05 = [0, 0]
    y_x_05 = [0, 0]
    x_05_controllato = []
    y_05_controllato = []

    #calcolo la differenza in X
    x_05[0] = (abs(matrix05[key][1][0] - matrix05[key][2][0]))
    x_05[1] = (abs(matrix05[key][3][0] - matrix05[key][4][0]))
    x_05[2] = (abs(matrix05[key][5][0] - matrix05[key][10][0]))
    x_05[3] = (abs(matrix05[key][7][0] - matrix05[key][8][0]))
    #calcolo la differenza in y per correggere la differenza in x (pitagora) con y_x si intende y di x
    y_x_05[0] = (abs(matrix05[key][1][1] - matrix05[key][2][1])) 
    y_x_05[1] = (abs(matrix05[key][3][1] - matrix05[key][4][1]))
    for j in range(len(x_05)):
        #controllo che le dimensioni non sfanculino
        if (x_05[j] < 36.5) and (x_05[j] > 35.5):
            x_05_controllato.append(np.sqrt(x_05[j]**2+np.mean(y_x_05)**2))
        #se la dimensione del lato non ha senso esco l'errore
        else:
            logging.warning("la misura del lato X è fuori dai parametri stabiliti (35.5 - 36.5), il suo valore è: %f \n Questo valore verrà scartato" % x_05[j])
    dimensione_lato_x_05[key] = np.mean(x_05_controllato)

    #calcolo la differenza in Y
    y_05[0] = (abs(matrix05[key][1][1] - matrix05[key][4][1]))
    y_05[1] = (abs(matrix05[key][2][1] - matrix05[key][3][1]))
    #calcolo la differenza in x per correggere la differenza in y (pitagora)
    x_y_05[0] = (abs(matrix05[key][1][0] - matrix05[key][4][0])) 
    x_y_05[1] = (abs(matrix05[key][2][0] - matrix05[key][3][0]))
    for i in range(len(y_05)):
        #controllo che le dimensioni non sfanculino
        if (y_05[i] < 705) and (y_05[i] > 703):
            y_05_controllato.append(np.sqrt(y_05[i]**2+np.mean(x_y_05)**2))
        #se la dimensione del lato non ha senso esco l'errore
        else:
            logging.warning("la misura del lato Y è fuori dai parametri stabiliti (703 - 705), il suo valore è: %f \n Questo valore verrà scartato" % y_05[i])
    dimensione_lato_y_05[key] = np.mean(y_05_controllato)



#controllo quanto sono dritti i traversini da 1mm
drittezza_lato1_1_10 = []
drittezza_lato1_2_10 = []
drittezza_lato2_1_10 = []
drittezza_lato2_2_10 = []
drittezza_media_lato1_10 = []
drittezza_media_lato2_10 = []

for key in matrix10:
    #fitto i punti di vertice dei due lati lungi
    y1 = [matrix10[key][1][0], matrix10[key][4][0]]
    y2 = [matrix10[key][2][0], matrix10[key][3][0]]
    x1 = [matrix10[key][1][1], matrix10[key][4][1]]
    x2 = [matrix10[key][2][1], matrix10[key][3][1]]
    coefficients1 = np.polyfit(x1, y1, 1)
    coefficients2 = np.polyfit(x2, y2, 1)
    
    #determino la drittezza dei lati nel senso di calcolare la distanza tra il punto teorico ricavato dal fit e il punto reale misurato prima lato1 (con x circa zero) e poi lato2
    y_teorico1_1 = matrix10[key][5][1]*coefficients1[0]+coefficients1[1]
    y_teorico1_2 = matrix10[key][7][1]*coefficients1[0]+coefficients1[1]
    drittezza_lato1_1_10.append(abs(matrix10[key][5][0]-y_teorico1_1))
    drittezza_lato1_2_10.append(abs(matrix10[key][7][0]-y_teorico1_2))
    drittezza_media_lato1_10.append((drittezza_lato1_1_10[key]+drittezza_lato1_2_10[key])/2)

    #lato2
    y_teorico2_1 = matrix10[key][8][1]*coefficients2[0]+coefficients2[1]
    y_teorico2_2 = matrix10[key][10][1]*coefficients2[0]+coefficients2[1]
    drittezza_lato2_1_10.append(abs(matrix10[key][8][0]-y_teorico2_1))
    drittezza_lato2_2_10.append(abs(matrix10[key][10][0]-y_teorico2_2))
    drittezza_media_lato2_10.append((drittezza_lato2_1_10[key]+drittezza_lato2_2_10[key])/2)


'''
plt.figure()
hist_drittezza = plt.hist(drittezza_lato1_1_10, bins=24)
plt.grid('on')
plt.show()
plt.figure()
hist_drittezza = plt.hist(drittezza_lato1_2_10, bins=24)
plt.grid('on')
plt.show()
plt.figure()
hist_drittezza = plt.hist(drittezza_lato2_1_10)
plt.grid('on')
plt.show()
plt.figure()
hist_drittezza = plt.hist(drittezza_lato2_2_10)
plt.grid('on')
plt.show()
'''

#controllo quanto sono dritti i traversini da 0.5mm
drittezza_lato1_1_05 = []
drittezza_lato1_2_05 = []
drittezza_lato2_1_05 = []
drittezza_lato2_2_05 = []
drittezza_media_lato1_05 = []
drittezza_media_lato2_05 = []

for key in matrix05:
    #fitto i punti di vertice dei due lati lungi
    y1 = [matrix05[key][1][0], matrix05[key][4][0]]
    y2 = [matrix05[key][2][0], matrix05[key][3][0]]
    x1 = [matrix05[key][1][1], matrix05[key][4][1]]
    x2 = [matrix05[key][2][1], matrix05[key][3][1]]
    coefficients1 = np.polyfit(x1, y1, 1)
    coefficients2 = np.polyfit(x2, y2, 1)
    
    #determino la drittezza dei lati nel senso di calcolare la distanza tra il punto teorico ricavato dal fit e il punto reale misurato prima lato1 (con x circa zero) e poi lato2
    y_teorico1_1 = matrix05[key][5][1]*coefficients1[0]+coefficients1[1]
    y_teorico1_2 = matrix05[key][7][1]*coefficients1[0]+coefficients1[1]
    drittezza_lato1_1_05.append(abs(matrix05[key][5][0]-y_teorico1_1))
    drittezza_lato1_2_05.append(abs(matrix05[key][7][0]-y_teorico1_2))
    drittezza_media_lato1_05.append((drittezza_lato1_1_05[key]+drittezza_lato1_2_05[key])/2)

    #lato2
    y_teorico2_1 = matrix05[key][8][1]*coefficients2[0]+coefficients2[1]
    y_teorico2_2 = matrix05[key][10][1]*coefficients2[0]+coefficients2[1]
    drittezza_lato2_1_05.append(abs(matrix05[key][8][0]-y_teorico2_1))
    drittezza_lato2_2_05.append(abs(matrix05[key][10][0]-y_teorico2_2))
    drittezza_media_lato2_05.append((drittezza_lato2_1_05[key]+drittezza_lato2_2_05[key])/2)
    
'''
print(dimensione_lato_x_10)
print(dimensione_lato_y_10)
print(dimensione_lato_x_05)
print(dimensione_lato_y_05)
'''

#disegno i vari grafici

#spessori 10
plt.figure()
plt.grid('on')
sns.distplot([matrix_spessori_10.mean(1)], hist=True, kde=False, bins=24, color='red')
sns.rugplot(data=matrix_spessori_10.mean(1), height=0.1, color='red')
plt.title('thickness 1.0 mm', fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.show()

#spessori 05
plt.figure()
plt.grid('on')
sns.distplot([matrix_spessori_05.mean(1)], hist=True, kde=False, bins=24, color='darkred')
sns.rugplot(data=matrix_spessori_05.mean(1), height=0.1, color='darkred')
plt.title('thickness 0.5 mm', fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.show()

#angolo di taglio 10 e 05
print('angoli05')
print(angolo_gradi_05)
plt.figure()
plt.grid('on')
sns.distplot([angolo_gradi_10], hist=True, kde=False, bins=12, color='green')
sns.rugplot(data=angolo_gradi_10, height=0.3, color='green', label='1.0 mm')
sns.distplot([angolo_gradi_05], hist=True, kde=False, bins=12, color='blue')
sns.rugplot(data=angolo_gradi_05, height=0.3, color='blue', label='0.5 mm')
plt.legend(fontsize=20)
plt.title('cutting angle', fontsize=20)
plt.xlabel('degrees', fontsize=14)
plt.show()

#stampa angolo di taglio 05
print('angoli05')
print(angolo_gradi_05)

'''
#geometria travetti
xpunto = []
ypunto = []

for key in matrix10:
    xpunto.append(matrix10[key][1][0])
    xpunto.append(matrix10[key][2][0])
    xpunto.append(matrix10[key][3][0])
    xpunto.append(matrix10[key][4][0])
    xpunto.append(matrix10[key][5][0])
    xpunto.append(matrix10[key][7][0])
    xpunto.append(matrix10[key][8][0])
    xpunto.append(matrix10[key][10][0])

    ypunto.append(matrix10[key][1][1])
    ypunto.append(matrix10[key][2][1])
    ypunto.append(matrix10[key][3][1])
    ypunto.append(matrix10[key][4][1])
    ypunto.append(matrix10[key][5][1])
    ypunto.append(matrix10[key][7][1])
    ypunto.append(matrix10[key][8][1])
    ypunto.append(matrix10[key][10][1])

plt.figure()
plt.grid('on')
scat_geometria = plt.scatter(xpunto, ypunto, marker='.')
plt.show()
'''

#drittezza media travetti 10 
print('drittezze10')
print(drittezza_media_lato1_10)
print(drittezza_media_lato2_10)

plt.figure()
plt.grid('on')
sns.distplot([drittezza_media_lato1_10], hist=True, kde=False, bins=12, color='blue', hist_kws={'range':(0,0.12)})
sns.distplot([drittezza_media_lato2_10], hist=True, kde=False, bins=12, color='red', hist_kws={'range':(0,0.12)})
sns.rugplot(data=drittezza_media_lato1_10, height=0.1, color='blue', label='left-side 1.0')
sns.rugplot(data=drittezza_media_lato2_10, height=0.1, color='red', label='right-side 1.0')
plt.legend(fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.title('straightness', fontsize=20)
plt.show()

#drittezza 05
print('drittezze05')
print(drittezza_media_lato1_05)
print(drittezza_media_lato2_05)

plt.figure()
plt.grid('on')
sns.distplot([drittezza_media_lato1_05], hist=True, kde=False, bins=12, color='darkblue', hist_kws={'range':(0,0.12)})
sns.distplot([drittezza_media_lato2_05], hist=True, kde=False, bins=12, color='darkred', hist_kws={'range':(0,0.12)})
sns.rugplot(data=drittezza_media_lato1_05, height=0.1, color='darkblue', label='left-side 0.5')
sns.rugplot(data=drittezza_media_lato2_05, height=0.1, color='darkred', label='right-side 0.5')
plt.legend(fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.title('straightness', fontsize=20)
plt.show()

#pesi 10
plt.figure()
plt.grid('on')
sns.distplot([matrix_pesi_10], hist=True, kde=False, bins=24, color='blue')
sns.rugplot(data=matrix_pesi_10, height=0.1, color='blue')
plt.title('weights 1.0 mm', fontsize=20)
plt.xlabel('grams', fontsize=15)
plt.show()

#pesi 05
plt.figure()
plt.grid('on')
sns.distplot([matrix_pesi_05], hist=True, kde=False, bins=24, color='darkblue')
sns.rugplot(data=matrix_pesi_05, height=0.1, color='darkblue')
plt.title('weights 0.5 mm', fontsize=20)
plt.xlabel('grams', fontsize=15)
plt.show()

#larghezze 10
plt.figure()
plt.grid('on')
sns.distplot([larghezza_lato_a_media10], hist=True, kde=False, bins=6, color='red')
sns.rugplot(data=larghezza_lato_a_media10, height=0.1, color='red', label='up-side')
sns.distplot([larghezza_lato_b_media10], hist=True, kde=False, bins=6, color='blue')
sns.rugplot(data=larghezza_lato_b_media10, height=0.1, color='blue', label='down-side')
plt.legend(fontsize=20)
plt.title('cutting width 1.0 mm', fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.show()

#larghezze 05
plt.figure()
plt.grid('on')
sns.distplot([larghezza_lato_a_media05], hist=True, kde=False, bins=6, color='darkred')
sns.rugplot(data=larghezza_lato_a_media05, height=0.1, color='darkred', label='up-side')
sns.distplot([larghezza_lato_b_media05], hist=True, kde=False, bins=6, color='darkblue')
sns.rugplot(data=larghezza_lato_b_media05, height=0.1, color='darkblue', label='down-side')
plt.legend(fontsize=20)
plt.title('cutting width 0.5', fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.show()

print('cutting width')
print(larghezza_lato_a_media05)
print(larghezza_lato_b_media05)