import numpy as np
import logging
import matplotlib.pyplot as plt
import seaborn as sns
import os

travegold = np.loadtxt('../contr_qualita_trave_gold.txt')

plt.scatter(travegold[0],travegold[1])