import numpy as np
import logging
import matplotlib.pyplot as plt
import seaborn as sns
import os

#carico i file e li inserisco dentro a delle matrici dentro ad un dizionario, divide tra quelli da 1mm e da 0.5mm
dirs = os.listdir('../structure_items_measurements/internal raw data/')
matrix_int_10_08 = {}
matrix_int_10_05= {}
k = 0
j = 0

for file in dirs:
    if file[5] == "0" and (file[6] == "1" or file[6] == "3" or file[6] == "5" or file[6] == "8"):
        matrix_int_10_08[k] = np.loadtxt('../structure_items_measurements/internal raw data/%s' % file, usecols=(1,2,3), skiprows=7, delimiter=',')
        k += 1
    else:
        matrix_int_10_05[j] = np.loadtxt('../structure_items_measurements/internal raw data/%s' % file, usecols=(1,2,3), skiprows=7, delimiter=',')
        j += 1

larghezza_interna_08 = []

for key in matrix_int_10_08:
    larghezza_interna_08.append(abs(matrix_int_10_08[key][1][0]-matrix_int_10_08[key][4][0]))
    larghezza_interna_08.append(abs(matrix_int_10_08[key][2][0]-matrix_int_10_08[key][3][0]))

larghezza_interna_05 = []

for key in matrix_int_10_05:
    larghezza_interna_05.append(abs(matrix_int_10_05[key][1][0]-matrix_int_10_05[key][4][0]))
    larghezza_interna_05.append(abs(matrix_int_10_05[key][2][0]-matrix_int_10_05[key][3][0]))

print("spessore 0.8 mm")
print(larghezza_interna_08)
print("spessore 0.5 mm")
print(larghezza_interna_05)

#internal width 10_08 and 05
plt.figure()
plt.grid('on')
sns.distplot([larghezza_interna_08], hist=True, kde=False, bins=18, color='red', hist_kws={'range':(33.925,34.075)})
sns.rugplot(data=larghezza_interna_08, height=0.1, color='red', label='0.8 thickness')
sns.distplot([larghezza_interna_05], hist=True, kde=False, bins=18, color='blue', hist_kws={'range':(33.925,34.075)})
sns.rugplot(data=larghezza_interna_05, height=0.1, color='blue', label='0.5 thickness')
plt.legend(fontsize=20)
plt.title('Internal Width 1.0 mm 0.8 mm Thickness', fontsize=20)
plt.xlabel('millimeters', fontsize=15)
plt.show()