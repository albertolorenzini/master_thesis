from mpl_toolkits.mplot3d import Axes3D as ax
import matplotlib.pyplot as plt
from root_pandas import read_root

df = read_root('GammaVertex_130521.root')

figure = plt.figure().gca(projection='3d')
figure.scatter(df.dx, df.dy, df.dz, s=0.1, c='black', marker=',')
figure.set_xlabel('x')
figure.set_ylabel('y')
figure.set_zlabel('z')

figure.set_xlim(-25,25)
figure.set_ylim(-25,25)
figure.set_zlim(-25,25)

plt.savefig('VertexFit3D_lim25.png', dpi=1200)