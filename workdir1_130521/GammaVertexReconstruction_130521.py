#!/usr/bin/env python3                                                                                                                       

# -*- coding: utf-8 -*-

import basf2 as b2
import modularAnalysis as ma
import stdV0s
import variables.collections as vc
import vertex
import variables
import variables.utils as vu


#Path creation

main = b2.create_path()


#Loading the root file

ma.inputMdstList(
	environmentType="default",
	filelist=[b2.find_file("EvtGenSimRecYesPairConversions.root")],
	path=main
)


#Decay reconstruction

ma.fillParticleList(
	"e+:uncorrected",
    cut="",
	path=main
)

ma.reconstructDecay(
    "gamma:ee -> e+:uncorrected e-:uncorrected",
    cut="",
    path=main
)


#Variables generation

b_variables = []

vertex.treeFit(
    'gamma:ee',
    ipConstraint=False,
    massConstraint=[22],
    path=main
)

standard_vars = vc.kinematics

tracking_vars = vc.event_level_tracking= ['omega', 'firstSVDLayer', 'firstCDCLayer', 'firstPXDLayer']


vertexfit = vc.vertex= ['x','y','z', 'distance']

b_variables += standard_vars
b_variables += tracking_vars
b_variables += vc.deltae_mbc
b_variables += vertexfit

ma.matchMCTruth("gamma:ee",path=main)

ma.fillParticleListFromMC("gamma:gen",
	cut="",
	addDaughters=True,
	path=main
)

b_variables += vc.mc_truth

#alias variables

fs_vars = standard_vars + tracking_vars + vc.mc_truth
b_variables += vu.create_aliases_for_selected(
    fs_vars,
    "gamma -> ^e+ ^e-",
    prefix=["ep", "em"]
)


#Variables to Ntuple

ma.variablesToNtuple(
	"gamma:ee",
	variables=b_variables,
	filename="GammaVertexee_200521.root",
	treename="tree",
	path=main,
)

ma.variablesToNtuple(
	"gamma:gen",
	variables=b_variables,
	filename="GammaVertexgen_200521.root",
	treename="tree",
	path=main,
)


#Processing

b2.process(main)

print(b2.statistics)