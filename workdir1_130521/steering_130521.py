#!/usr/bin/env python3                                                                                                                       

# -*- coding: utf-8 -*-                                                                                                                      



import basf2 as b2

import sys
import modularAnalysis as ma
import stdV0s
import variables.collections as vc

import simulation as si

import reconstruction as re



main = b2.create_path()



# Create Event information                                                                                                                   

eventinfosetter = b2.register_module('EventInfoSetter')

main.add_module(eventinfosetter)





particlegun = b2.register_module('ParticleGun')

particlegun.param('pdgCodes', [22])



main.add_module(particlegun)



# add simulation                                                                                                                             

si.add_simulation(main)



# add reconstruction                                                                                                                         

re.add_reconstruction(main)



# write root output                                                                                                                          

main.add_module("RootOutput")



main.add_module("Progress")


ma.inputMdstList(
	environmentType="default",
	filelist=[b2.find_file("RootOutput.root")],
	path=main
)



b2.print_path(main)



b2.process(main)